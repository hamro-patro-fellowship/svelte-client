
#!/bin/bash

# proto-loader-gen-types --grpcLib=@grpc/grpc-js --outDir= ./Backend/Proto/ ./Backend/Proto/*.proto

#path to this plugin
# PATH_TO_GEN_TS = "./node_modules/.bin/protoc-gen-ts"

mkdir -p ./Frontend/src/GeneratedTS/
  # --plugin="protoc-gen-ts=${PATH_TO_GEN_TS}" \
  # --grpc-web_out=import_style=typescript,mode=grpcwebtext:./Frontend/src/GeneratedTS/ \




  # --grpc-web_out=import_style=typescript,mode=grpcwebtext:./Frontend/src/GeneratedTS/ \
  # --ts_out=service=grpc-web,mode=grpc-js:./Frontend/src/GeneratedTS/Frontend/src

protoc -I=. ./Frontend/src/Proto/*.proto \
  --js_out=import_style=commonjs:./Frontend/src/GeneratedTS \
  --grpc-web_out=import_style=typescript,mode=grpcweb:./Frontend/src/GeneratedTS/ 
