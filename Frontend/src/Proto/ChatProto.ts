import type * as grpc from '@grpc/grpc-js';
import type { MessageTypeDefinition } from '@grpc/proto-loader';

import type { ChatAppClient as _chatProto_ChatAppClient, ChatAppDefinition as _chatProto_ChatAppDefinition } from './chatProto/ChatApp';

type SubtypeConstructor<Constructor extends new (...args: any) => any, Subtype> = {
  new(...args: ConstructorParameters<Constructor>): Subtype;
};

export interface ProtoGrpcType {
  chatProto: {
    ChatApp: SubtypeConstructor<typeof grpc.Client, _chatProto_ChatAppClient> & { service: _chatProto_ChatAppDefinition }
    allChatRequest: MessageTypeDefinition
    allChatResponse: MessageTypeDefinition
  }
}

