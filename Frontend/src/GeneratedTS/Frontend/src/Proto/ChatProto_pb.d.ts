import * as jspb from 'google-protobuf'



export class allChatRequest extends jspb.Message {
  getId(): number;
  setId(value: number): allChatRequest;

  getMessage(): string;
  setMessage(value: string): allChatRequest;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): allChatRequest.AsObject;
  static toObject(includeInstance: boolean, msg: allChatRequest): allChatRequest.AsObject;
  static serializeBinaryToWriter(message: allChatRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): allChatRequest;
  static deserializeBinaryFromReader(message: allChatRequest, reader: jspb.BinaryReader): allChatRequest;
}

export namespace allChatRequest {
  export type AsObject = {
    id: number,
    message: string,
  }
}

export class allChatResponse extends jspb.Message {
  getId(): number;
  setId(value: number): allChatResponse;

  getMessage(): string;
  setMessage(value: string): allChatResponse;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): allChatResponse.AsObject;
  static toObject(includeInstance: boolean, msg: allChatResponse): allChatResponse.AsObject;
  static serializeBinaryToWriter(message: allChatResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): allChatResponse;
  static deserializeBinaryFromReader(message: allChatResponse, reader: jspb.BinaryReader): allChatResponse;
}

export namespace allChatResponse {
  export type AsObject = {
    id: number,
    message: string,
  }
}

